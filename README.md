# Docker dev stack for symfony app

* PHP 7.4
* Nginx
* MYSQL 8
* Node
* Phpmyadmin

## First time
If you are runing this for a first time you need to download containers

```shell
docker-compose up -d --build
```

## Run docker containers

 In order to start with a project open terminal in project root and run
 
```shell
sudo docker-compose up -d
```

## SSH as root
If you want to run symfony CLI and install some bundles i.e. api (api platform) yon need to login as root user

```shell
sudo docker exec --user root -it php74_container bash
```

## Stop docker containers
```shell
sudo docker-compose stop
```

## List all running containers
```shell
sudo docker-compose ps
```
## SSH into the php container
```shell
sudo docker exec -it php74_container bash
```
## In order to install a symfony project go to
```shell
sudo docker exec -it php74-container bash
rm -rf public
composer create-project symfony/skeleton .
```
In www dir run command
```shell
symfony new .
```
This command will download all files from composer

## Stop and remove container
``` shell
   sudo docker stop nginx_container && sudo docker rm nginx_container
```   


# Run in browser
### Project
http://localhost:8080

### Phpmyadmin
http://localhost:8090
